#include <iostream>
#include <fstream>
#include <algorithm>

int citire(int sir[])
{
	std::ifstream fin("examen1.txt");
	int x, n = 0;
	while (fin >> x)
		sir[n++] = x;
	return n;
}

void prelucrare(int sir[], int n)
{
	for (int index = 0; index < n / 2; index += 2)
	{
		int temp = sir[index + n / 2];
		for (int index2 = n / 2 - 1; index2 > index; index2--)
			sir[index2 + 1] = sir[index2];
		sir[index + 1] = temp;
	}
}

void afisare(int sir[], int n)
{
	std::ofstream fout("examen2.txt");
	int m = n;
	if (n % 2 == 1)
		m--;
	for (int index = 0; index < m; index++)
		fout << sir[index] << " ";
	fout << std::endl;
	fout << "Metoda folosita este Greedy";
}

int main()
{
	int *sir = new int;
	int n = citire(sir);
	std::sort(sir, sir + n);
	prelucrare(sir, n);
	afisare(sir, n);
	delete sir;
	system("pause");
	return 0;
}