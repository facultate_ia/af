﻿/*5. Se dă o bucată de hârtie de dimensiune A x B. Hârtia se taie în pătrate de orice dimensiune. 
Găsiți numărul minim de pătrate în care se poate tăia hârtia.
Exemplu 13 x 29 = 2 (13x13) + 4 (3x3) + 3 (1x1) = 9 bucăți*/

#include <iostream>

int taiere_hartie(int dimA, int dimB)
{
	int nrTaieturi = 0;
	while (dimA > 0 && dimB > 0)
	{
		if (dimA < dimB && dimA != 0)
		{
			nrTaieturi += dimB / dimA;
			dimB = dimB % dimA;
		}
		if (dimB < dimA && dimB != 0)
		{
			nrTaieturi += dimA / dimB;
			dimA = dimA % dimB;
		}
	}
	return nrTaieturi;
}

int main()
{
	int dimA, dimB;
	std::cin >> dimA >> dimB;
	std::cout << taiere_hartie(dimA, dimB);
	system("pause");
	return 0;
}