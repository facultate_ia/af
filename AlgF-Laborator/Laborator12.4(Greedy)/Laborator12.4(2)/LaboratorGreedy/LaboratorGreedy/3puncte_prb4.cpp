﻿/*4. Se dă o fracție. Să se exprime ca sumă minimă de alte fracții pozitive cu numărătorul 1.
Exemplu 6/14 este 1/3 + 1/11 + 1/231 sau 12/13 este 1/2 + 1/3 + 1/12 + 1/156.*/

#include <iostream>

struct Fractie
{
	int numarator;
	int numitor;
};

void suma_min_fractii(Fractie fractie)
{
	if (fractie.numarator == 0)
		return;
	int intermediar = fractie.numitor / fractie.numarator;
	if (intermediar != (float)fractie.numitor / fractie.numarator)
		intermediar++;
	fractie.numarator = fractie.numarator*intermediar - fractie.numitor;
	fractie.numitor = fractie.numitor*intermediar;
	std::cout << " 1 / " << intermediar;
	if (fractie.numarator != 0)
		std::cout << " + ";
	suma_min_fractii(fractie);
}

int main()
{
	Fractie fractie;
	std::cout << "Introduceti numaratorul: ";
	std::cin >> fractie.numarator;
	std::cout << "Introduceti numitorul: ";
	std::cin >> fractie.numitor;
	suma_min_fractii(fractie);
	std::cout << std::endl;
	system("pause");
	return 0;
}