/*3. La o casa de marcat sunt serviti n clienti. Cunoscandu-se timpul de servire pt fiecare client, 
sa se determine o ordine de servire astfel incat timpul mediu sa fie minim. Cate posibile ordini de servire exista?
n=3    t1=5   t2=10    t3=3
t1+t2+t3 = 5+5+10+5+10+3 = 38
t1+t3+t2 = 5+5+3+5+3+10 = 31
t2+t1+t3 = 10+10+5+10+5+3 = 43
t2+t3+t1 = 10+10+3+10+3+5 = 41
t3+t1+t2 = 3+3+5+3+5+10 = 29  solutia optima
t3+t2+t1 = 3+3+10+3+10+5 = 34*/
#include <iostream>
#include <algorithm>

void citire(int timpi[10], unsigned int &n)
{
	std::cin >> n;
	for (int index = 0; index < n; index++)
		std::cin >> timpi[index];
}

int main()
{
	unsigned int n;
	int timpi[10];
	citire(timpi, n);
	std::sort(timpi, timpi + n);
	for (int index = 0; index < n; index++)
		std::cout << timpi[index] << " ";
	system("pause");
	return 0;
}