/*2. Sa se plateasca o suma S cu un nr minim de bancnote cu valori date. 
Se considera ca din fiecare tip de bancnota sa poate folosi un nr nelimitat de bancnote, 
iar pt ca problema sa aiba solutie mereu, vom considera ca exista bancnote de valoarea 1.
Valori bancnote: 5, 1, 50, 10
Suma de platit: 147 lei
Solutia optima: 50*2 + 10*4 + 5*1 + 1*2*/
#include <iostream>
#include <fstream>
#include <algorithm>

bool comparator(int x, int y)
{
	return x > y;
}

void citire(int bancnote[], unsigned int &noBancnote, int &suma)
{
	std::ifstream fin("C:\\Users\\ro_ad\\Desktop\\AlgF\\Laborator12.4(Greedy)\\Laborator12.4\\Laborator12.4\\in.txt");
	fin >> noBancnote;
	for (int index = 0; index < noBancnote; index++)
		fin >> bancnote[index];
	fin >> suma;
}

void solutie(int bancnote[], unsigned int noBancnote, int suma)
{
	for (int index = 0; index < noBancnote; index++)
	{
		int nr = 0;
		while (suma - bancnote[index] >= 0)
		{
			nr++;
			suma = suma - bancnote[index];
		}
		std::cout << bancnote[index] << " * " << nr << "  ";
	}
	std::cout << std::endl;
}

int main()
{
	int bancnote[10], suma;
	unsigned int noBancnote;
	citire(bancnote, noBancnote, suma);
	std::sort(bancnote, bancnote + noBancnote, comparator);
	solutie(bancnote, noBancnote, suma);
	system("pause");
	return 0;
}