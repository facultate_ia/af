﻿/*1. Se da o multime A cu elemnetele a1 pana la an, nr reale. Sa se determine o submultime S,
astfel incat suma elementelor submultimii sa fie maxima.
A={12, -4, 78, -21, 5, 18} 
S={12, 78, 5, 18}
A={-12, -7, -895, -50, -230}
S=Φ*/
#include <iostream>
#include <fstream>

void citire(double multime[], unsigned int &dim)
{
	std::ifstream fin("in.txt");
	fin >> dim;
	for (int index = 0; index < dim; index++)
		fin >> multime[index];
}

void solutie(double multime[], unsigned int dim)
{
	double suma = 0;
	for(int index=0;index<dim;index++)
		if (multime[index] >= 0)
		{
			std::cout << multime[index]<<" ";
			suma += multime[index];
		}
	std::cout << std::endl << "Suma este: " << suma << std::endl;
}

int main()
{
	double multime[100];
	unsigned int dim;
	citire(multime, dim);
	solutie(multime, dim);
	system("pause");
	return 0;
}