﻿/*1. Se dă un vector convex (elementele sale sunt în ordine strict crescătoare până la o
anumită poziție, după care sunt strict descrescătoare)
Exemplu: v ={1,2,3,4,5,6,4,3,2}
Să se determine maximul din vector cu ajutorul unui algoritm de tip „divide et impera”.*/
#include <iostream>

void citire(int *vector,unsigned int dim)
{
	for (int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

int maxim_vector_convex(int *vector, int start, int end)
{
	if (start >= end)
		return vector[start];
	int mij = (start + end) / 2;
	if (vector[mij] < vector[mij - 1])
		return maxim_vector_convex(vector, start, mij - 1);
	if (vector[mij] < vector[mij + 1])
		return maxim_vector_convex(vector, mij + 1, end);
	if (vector[mij]< vector[mij + 1] && vector[mij]>vector[mij - 1])
		return vector[mij];
}

int main()
{
	unsigned int dim;
	int *vector;
	std::cin >> dim;
	vector = new int[dim];
	citire(vector, dim);
	std::cout << maxim_vector_convex(vector, 0, dim - 1);
	system("pause");
	return 0;
}