#include <iostream>

void citire(unsigned int &dim, int vector[])
{
	std::cin >> dim;
	for (int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

int cautare01(unsigned int dim, int vector[], int start, int end)
{
	if (start > end)
		return 0;
	int mij = (start + end) / 2;
	if (vector[mij] == 1)
	{
		if (vector[mij + 1] == 0)
			return dim - mij - 1;
		return cautare01(dim, vector, mij + 1, end);
	}
	if (vector[mij] == 0)
	{
		if (vector[mij - 1] == 1)
			return dim - mij;
		return cautare01(dim, vector, start, mij - 1);
	}
}

int main()
{
	unsigned int dim;
	int vector[100];
	citire(dim, vector);
	std::cout << cautare01(dim, vector, 0, dim - 1);
	system("pause");
	return 0;
}