// AdunareBinara.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

void baza2(unsigned int x, unsigned int sir[], unsigned int &m)
{
	unsigned int aux = 0;
	while (x)
	{
		sir[m++] = x % 2;
		x /= 2;
	}
}

void afisare(unsigned int s[],unsigned int dim)
{
	for (unsigned int i = 0; i < dim; i++)
		std::cout << s[i] << " ";
}

void inversare(unsigned int s[], unsigned int dim)
{
	for (unsigned int st = 0, dr = dim - 1; st < dr; st++, dr--)
	{
		unsigned int aux = s[st];
		s[st] = s[dr];
		s[dr] = aux;
	}
}

void adaugare0(unsigned int sir[], unsigned int& dim1, unsigned int dim2)
{
	for (unsigned int i = dim1; i < dim2; i++)
		sir[i] = 0;
	dim1 = dim2;
}

int main()
{
	unsigned int nr1, nr2, numar1[1000], numar2[1000], suma[1000], dim1 = 0, dim2 = 0, t = 0;
	std::cout << "Primul nr= ";
	std::cin >> nr1;
	std::cout << "Al doilea nr= ";
	std::cin >> nr2;
	baza2(nr1, numar1, dim1);
	baza2(nr2, numar2, dim2);
	if (dim1 < dim2)
		adaugare0(numar1, dim1, dim2);
	else
		adaugare0(numar2, dim2, dim1);
	for (unsigned int i = 0; i < dim1; i++)
	{
		suma[i] = (numar1[i] + numar2[i] + t) % 2;
		t = (numar1[i] + numar2[i] + t) / 2;
	}
	if (t)
	{
		suma[dim1] = t;
		dim1++;
	}
	inversare(suma, dim1);
	std::cout << "\n";
	std::cout << "Suma celor doua nr in baza 2 este: ";
	afisare(suma, dim1);
	std::cout << "\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
