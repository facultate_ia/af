// ExistaCMMDCvector.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

void citire(unsigned int v[], unsigned int &dim)
{
	std::cin >> dim;
	for (unsigned int i = 0; i < dim; i++)
		std::cin >> v[i];
}

unsigned int cmmdc(unsigned int x, unsigned int y)
{
	unsigned int r;
	while (y)
	{
		r = x % y;
		x = y;
		y = r;
	}
	return x;
}

bool verific(unsigned int vector[], unsigned int dim)
{
	for (unsigned int i = 0; i < dim; i++)
		for (unsigned int j = 0; j < dim - 1 && j != i; j++)
			for (unsigned int k = j + 1; k < dim && k != i && k != j; k++)
				if (i == cmmdc(vector[j], vector[k]))

					return 1;
	return 0;
}

int main()
{
	unsigned int vector[1000], dim, c = 0;
	citire(vector, dim);
	if (verific(vector, dim))
		std::cout << "DA";
	else
		std::cout << "NU";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
