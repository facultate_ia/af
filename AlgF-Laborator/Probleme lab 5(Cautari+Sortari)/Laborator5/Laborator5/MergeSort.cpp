#include <iostream>

void citire(int vector[], unsigned int &dim)
{
	std::cin >> dim;
	for (int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

void interclasare(int vector_aux[100], int tablou1[], unsigned int dim1, int tablou2[], unsigned int dim2)
{
	int dim3 = 0, index1, index2;
	for (index1 = 0, index2 = 0; index1 < dim1 && index2 < dim2;)
		if (tablou1[index1] < tablou2[index2])
			vector_aux[dim3++] = tablou1[index1++];
		else
			vector_aux[dim3++] = tablou2[index2++];
	for (; index1 < dim1; index1++)
		vector_aux[dim3++] = tablou1[index1];
	for (; index2 < dim2; index2++)
		vector_aux[dim3++] = tablou1[index2];
}

void merge_sort(int vector[], unsigned int dim)
{
	int mij, start, end, tablou1[50], tablou2[50];
	if (dim > 1)
	{
		mij = dim / 2;
		start = mij;
		end = dim - mij;
		for (int index = 0; index < start; index++)
			tablou1[index] = vector[index];
		for (int index = 0; index < end; index++)
			tablou1[index] = vector[start + index];
		merge_sort(tablou1, start);
		merge_sort(tablou2, end);
		int vector_aux[100];
		interclasare(vector_aux, tablou1, start, tablou2, end);
		for (int index = 0; index < dim; index++)
			vector[index] = vector_aux[index];
	}
}

void afisare(int vector[], unsigned int dim)
{
	for (int index = 0; index < dim; index++)
		std::cout << vector[index] << " ";
	std::cout << std::endl;
}

int main()
{
	int vector[100];
	unsigned int dim;
	citire(vector, dim);
	merge_sort(vector, dim);
	afisare(vector, dim);
	system("pause");
	return 0;
}