#include <iostream>

void citire(int vector[], unsigned int &dim)
{
	std::cin >> dim;
	for (int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

void bubble_sort(int vector[], unsigned int dim)
{
	bool modific = true;
	while (modific == true)
	{
		modific = false;
		for (int index = 0; index < dim - 1; index++)
			if (vector[index] >vector[index+1])
			{
				int auxiliar = vector[index];
				vector[index] = vector[index+1];
				vector[index+1] = auxiliar;
				modific = true;
			}
	}
}

void afisare(int vector[], unsigned int dim)
{
	for (int index = 0; index < dim; index++)
		std::cout << vector[index] << " ";
	std::cout << std::endl;
}

int main()
{
	int vector[100];
	unsigned int dim;
	citire(vector, dim);
	bubble_sort(vector, dim);
	afisare(vector, dim);
	system("pause");
	return 0;
}