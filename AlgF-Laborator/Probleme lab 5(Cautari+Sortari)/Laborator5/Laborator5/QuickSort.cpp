#include <iostream>

int vector[100];

void citire(int vector[], unsigned int &dim)
{
	std::cin >> dim;
	for (int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

void quick_sort(int stanga, int dreapta)
{
	int index1 = stanga, index2 = dreapta, x = vector[(stanga + dreapta) / 2];
	do {
		while (vector[index1] < x)
			index1++;
		while (vector[index2] > x)
			index2--;
		if (index1 <= index2)
		{
			int aux = vector[index1];
			vector[index1] = vector[index2];
			vector[index2] = aux;
			index1++;
			index2--;
		}
	} while (index1 <= index2);
	if (stanga < index2)
		quick_sort(stanga, index2);
	if (dreapta > index1)
		quick_sort(index1, dreapta);
}

void afisare(int vector[], unsigned int dim)
{
	for (int index = 0; index < dim; index++)
		std::cout << vector[index] << " ";
	std::cout << std::endl;
}

int main()
{
	unsigned int dim;
	citire(vector, dim);
	quick_sort(0, dim - 1);
	afisare(vector, dim);
	system("pause");
	return 0;
}