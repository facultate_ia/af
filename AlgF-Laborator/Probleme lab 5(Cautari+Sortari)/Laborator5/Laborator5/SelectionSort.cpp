#include <iostream>

void citire(int vector[], unsigned int &dim)
{
	std::cin >> dim;
	for (int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

void selection_sort(int vector[], unsigned int dim)
{
	
	for (int index1 = 0; index1 < dim - 1; index1++)
	{
		int minim = vector[index1], pozitie = index1;
		for (int index2 = index1 + 1; index2 < dim; index2++)
			if (vector[index2] < minim)
			{
				minim = vector[index2];
				pozitie = index2;
			}
		int auxiliar = vector[index1];
		vector[index1] = vector[pozitie];
		vector[pozitie] = auxiliar;
	}
}

void afisare(int vector[], unsigned int dim)
{
	for (int index = 0; index < dim; index++)
		std::cout << vector[index] << " ";
	std::cout << std::endl;
}

int main()
{
	int vector[100];
	unsigned int dim;
	citire(vector, dim);
	selection_sort(vector, dim);
	afisare(vector, dim);
	system("pause");
	return 0;
}