/*1. Sa se scrie metode pentru cautarea secventiala si cautarea binara (recursiv si iterative).*/
#include <iostream>

void citire(int vector[], unsigned int &dim)
{
	std::cin >> dim;
	for (int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

bool caut_secv_iterativ(int vector[],unsigned int dim,int x)
{
	for (int index = 0; index < dim; index++)
		if (vector[index] == x)
			return true;
	return false;
}

bool caut_secv_recursiv(int vector[], unsigned int dim, int x, int index)
{
	if (index >= dim)
		return false;
	if (vector[index] == x)
		return true;
	return caut_secv_recursiv(vector, dim, x, index + 1);
}

bool caut_bin_iterativ(int vector[], unsigned int dim, int x)
{
	int start = 0, end = dim - 1, mij;
	while (start <= end)
	{
		mij = (start + end) / 2;
		if (vector[mij] == x)
			return true;
		else
			if (vector[mij] < x)
				start = mij + 1;
			else
				end = mij - 1;
	}
	return false;
}

bool caut_bin_recursiv(int vector[], unsigned int dim, int x, int start, int end)
{
	if (start > end)
		return false;
	int mij = (start + end) / 2;
	if (vector[mij] == x)
		return true;
	if (vector[mij] < x)
		return caut_bin_recursiv(vector, dim, x, mij + 1, end);
	return caut_bin_recursiv(vector, dim, x, start, mij - 1);
}

int main()
{
	int vector[100], x;
	unsigned int dim;
	citire(vector, dim);
	std::cin >> x;
	if (caut_bin_recursiv(vector, dim, x, 0, dim - 1))
		std::cout << "DA";
	else
		std::cout << "NU";
	system("pause");
	return 0;
}