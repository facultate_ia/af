/*25. Se citeste un numar natural x.Sa se afiseze cel mai mare divisor comun al cifrelor lui.*/
#include <iostream>

unsigned int cmmdc(int x, int y)
{
	unsigned int r = 0;
	while (y)
	{
		r = x % y;
		x = y;
		y = r;
	}
	return x;
}

unsigned int cmmdc_cifre(int x)
{
	unsigned int c = 0;
	while (x)
	{
		cmmdc(c, x % 10);
		x /= 10;
	}
	return c;
}

int main()
{
	unsigned int x;
	std::cin >> x;
	std::cout << cmmdc_cifre(x);
	system("pause");
	return 0;
}