﻿/*1. Se citesc n numere întregi. Să se realizeze suma numerelor pare şi produsul celor impare.*/

#include <iostream>

int main()
{
	unsigned int n, suma = 0, produs = 1;
	int x;
	std::cin >> n;
	while (n)
	{
		std::cin >> x;
		if (x % 2 == 0)
			suma = suma + x;
		else
			produs = produs * x;
		n--;
	}
	std::cout << "Suma nr pare: " << suma;
	std::cout << "\n";
	std::cout << "Produs nr impare: " << produs;
	std::cout << "\n";
	system("pause");
	return 0;
}