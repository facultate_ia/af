﻿/*14. Se dă un vector v cu n elemente. Să se elimine din vector elementul de pe poziția p (citită de la tastatură).*/
#include <iostream>

void citire(int vector[], unsigned int &length)
{
	std::cin >> length;
	for (unsigned int i = 0; i < length; i++)
		std::cin >> vector[i];
}

void sterg(int vector[], unsigned int &length, unsigned int position)
{
	for (unsigned int i = position ; i < length; i++)
		vector[i] = vector[i + 1];
	length--;
}

void afisare(unsigned int length, int v[])
{
	for (unsigned int i = 0; i < length; i++)
		std::cout << v[i] << " ";
}

int main()
{
	unsigned int length, position;
	int vector[1000];
	citire(vector, length);
	std::cout << "Position: ";
	std::cin >> position;
	sterg(vector, length, position);
	std::cout << "\n";
	afisare(length, vector);
	system("pause");
	return 0;
}