﻿/*24. Într-un şir de numere reale ordonate să se insereze la poziţia corectă în şir media aritmetică a elementelor acestuia.*/
#include <iostream>

void citire(float vector[], unsigned int &length)
{
	std::cin >> length;
	for (unsigned int i = 0; i < length; i++)
		std::cin >> vector[i];
}

void afisare(unsigned int length, float vector[])
{
	for (unsigned int i = 0; i < length; i++)
		std::cout << vector[i] << " ";
}

float media(float vector[],unsigned int length)
{
	float suma = 0;
	for (unsigned int i = 0; i < length; i++)
		suma += vector[i];
	return suma / length;
}

void inserare(float vector[], unsigned int &length,float medie)
{
	unsigned int position;
	for (unsigned int i = 0; i < length; i++)
		if (medie < vector[i])
			position = i;
	unsigned int i;
	for (i = length - 1; i >= position; i--)
		vector[i + 1] = vector[i];
	vector[i] = medie;
	length++;
}

int main()
{
	unsigned int length;
	float vector[1000], medie;
	citire(vector, length);
	medie = media(vector, length);
	afisare(length, vector);
	system("pause");
	return 0;
}