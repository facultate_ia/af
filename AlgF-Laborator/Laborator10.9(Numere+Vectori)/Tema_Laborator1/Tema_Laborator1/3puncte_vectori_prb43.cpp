﻿/*43. Suma a două numere în baza 2.*/

#include <iostream>

void baza2(unsigned int x, unsigned int sir[], unsigned int &m)
{
	unsigned int aux = 0;
	while (x)
	{
		sir[m++] = x % 2;
		x /= 2;
	}
}

void afisare(unsigned int s[], unsigned int dim)
{
	for (unsigned int i = 0; i < dim; i++)
		std::cout << s[i] << " ";
}

void inversare(unsigned int s[], unsigned int dim)
{
	for (unsigned int st = 0, dr = dim - 1; st < dr; st++, dr--)
	{
		unsigned int aux = s[st];
		s[st] = s[dr];
		s[dr] = aux;
	}
}

void adaugare0(unsigned int sir[], unsigned int& dim1, unsigned int dim2)
{
	for (unsigned int i = dim1; i < dim2; i++)
		sir[i] = 0;
	dim1 = dim2;
}

int main()
{
	unsigned int nr1, nr2, numar1[100], numar2[100], suma[102], dim1 = 0, dim2 = 0, t = 0;
	std::cout << "Primul nr= ";
	std::cin >> nr1;
	std::cout << "Al doilea nr= ";
	std::cin >> nr2;
	baza2(nr1, numar1, dim1);
	baza2(nr2, numar2, dim2);
	if (dim1 < dim2)
		adaugare0(numar1, dim1, dim2);
	else
		adaugare0(numar2, dim2, dim1);
	for (unsigned int i = 0; i < dim1; i++)
	{
		suma[i] = (numar1[i] + numar2[i] + t) % 2;
		t = (numar1[i] + numar2[i] + t) / 2;
	}
	if (t)
	{
		suma[dim1] = t;
		dim1++;
	}
	inversare(suma, dim1);
	std::cout << "\n";
	std::cout << "Suma celor doua nr in baza 2 este: ";
	afisare(suma, dim1);
	std::cout << "\n";
	system("pause");
	return 0;
}