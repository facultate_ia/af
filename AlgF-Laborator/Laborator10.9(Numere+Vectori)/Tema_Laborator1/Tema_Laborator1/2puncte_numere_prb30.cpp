﻿/*30. Să se citească pe rând n numere întregi şi să se caluleze primele două maxime (fără a reţine valorile într-un vector).*/

#include <iostream>

int main()
{
	unsigned int n;
	int x, maxim1 = INT_MIN, maxim2 = INT_MIN;
	std::cout << "Introduceti nr de elemente: ";
	std::cin >> n;
	for (unsigned int index = 0; index < n; index++)
	{
		std::cin >> x;
		if (x > maxim1)
		{
			maxim2 = maxim1;
			maxim1 = x;
		}
		else
			if (x > maxim2)
				maxim2 = x;
	}
	std::cout << "Primul maxim este: " << maxim1 << "\nAl doilea maxim este: " << maxim2 << "\n";
	system("pause");
	return 0;
}