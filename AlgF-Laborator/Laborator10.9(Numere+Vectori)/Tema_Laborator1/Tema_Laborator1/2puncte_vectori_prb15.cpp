﻿/*15. Să se interclaseze doi vectori de numere, ordonaţi strict crescător.*/

#include <iostream>

void citire(int vector[], unsigned int dim)
{
	for (unsigned int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

void interclasare(int vector1[], int vector2[], int interclasat[], unsigned int dim1, unsigned int dim2, unsigned int &dim3)
{
	int index1 = 0, index2 = 0;
	while (index1 < dim1 && index2 < dim2)
	{
		if (vector1[index1] < vector2[index2])
			interclasat[dim3++] = vector1[index1++];
		else
			interclasat[dim3++] = vector2[index2++];
	}
	for (; index1 < dim1; index1++)
		interclasat[dim3++] = vector1[index1];
	for (; index2 < dim2; index2++)
		interclasat[dim3++] = vector2[index2];
}

void afisare(int vector[], unsigned int dim)
{
	for (unsigned int index = 0; index < dim; index++)
		std::cout << vector[index] << " ";
	std::cout << "\n";
}

int main()
{
	unsigned int dim1, dim2, dim3;
	int	vector1[100], vector2[100], interclasat[200];
	std::cout << "Introduceti dimensiunea primului vector: ";
	std::cin >> dim1;
	std::cout << "\nIntroduceti dimensiunea celui de-al doilea vector: ";
	std::cin >> dim2;
	std::cout << "Introduceti elementele primului vector: \n";
	citire(vector1, dim1);
	std::cout << "Introduceti elementele celui de-al doilea vector: \n";
	citire(vector2, dim2);
	interclasare(vector1, vector2, interclasat, dim1, dim2, dim3);
	std::cout << "Interclasarea celor doi vectori este: \n";
	afisare(interclasat, dim3);
	system("pause");
	return 0;
}