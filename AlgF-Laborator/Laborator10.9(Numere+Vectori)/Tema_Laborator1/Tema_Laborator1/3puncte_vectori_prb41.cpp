﻿/*41. Suma/diferenţa/produsul a două numere foarte mari, memorate într-un vector.*/

#include <iostream>
#include <cstring> 

void citire(unsigned int numar[])
{
	char string[50];
	std::cin >> string;
	numar[0] = strlen(string);
	for (unsigned int index1 = 1, index2 = numar[0]; index1 < numar[0]; index1++, index2--)
		numar[index2] = string[index1] - '0';
}

void suma_nr(unsigned int numar1[], unsigned int numar2[], unsigned int suma[])
{
	suma[0] = (numar1[0] > numar2[0]) ? numar1[0] : numar2[0];
	unsigned int transport = 0;
	for (unsigned int index = 1; index < suma[0]; index++)
	{
		suma[index] = (numar1[index] + numar2[index] + transport) % 10;
		transport = (numar1[index] + numar2[index] + transport) / 10;
	}
	if (transport)
		suma[++suma[0]] = transport;
}

void afisare(unsigned int suma[])
{
	for (int index = suma[0]; index > 0; index--)
		std::cout << suma[index];
	std::cout << "\n";
}

int main()
{
	unsigned int numar1[50], numar2[50],suma[51];
	citire(numar1);
	citire(numar2);
	suma_nr(numar1, numar2, suma);
	afisare(suma);
	system("pause");
	return 0;
}