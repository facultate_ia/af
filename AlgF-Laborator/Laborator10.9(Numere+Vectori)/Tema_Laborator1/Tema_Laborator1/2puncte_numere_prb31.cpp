/*31. Sa se afiseze cel mai mare numar care se poate forma din cifrele lui x, luate o singura data.*/

#include <iostream>

void cifre(unsigned int x, unsigned int frecventa[])
{
	do
	{
		frecventa[x % 10]++;
		x /= 10;
	} while (x);
}

unsigned int formare(unsigned int frecventa[])
{
	unsigned int nr_nou = 0;
	for (int index = 9; index >= 0; index--)
		if (frecventa[index])
			nr_nou = nr_nou * 10 + index;
	return nr_nou;
}

int main()
{
	unsigned int x, frecventa[10], nr_nou;
	for (unsigned int index = 0; index < 10; index++)
		frecventa[index] = 0;
	std::cout << "Introduceti nr x: ";
	std::cin >> x;
	std::cout << "\n";
	cifre(x, frecventa);
	nr_nou = formare(frecventa);
	std::cout << "Cel mai mare nr format din cifrele nr x este: " << nr_nou << "\n";
	system("pause");
	return 0;
}