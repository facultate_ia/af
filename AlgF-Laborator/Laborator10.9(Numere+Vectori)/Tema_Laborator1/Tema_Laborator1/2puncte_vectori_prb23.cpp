﻿/*23. Memorând coeficienţii a două polinoame sub formă de vetori se cere să se calculeze
produsul celor două polinoame.*/

#include <iostream>

void citire(float polinom[], unsigned int &grad)
{
	std::cout << "Introduceti gradul polinomului: ";
	std::cin >> grad;
	std::cout << "\n";
	std::cout << "Introduceti coeficientii polinomului: ";
	std::cout << "\n";
	for (unsigned int index = 0; index <= grad; index++)
		std::cin >> polinom[index];
}

void produs_polin(float polinom1[], unsigned int grad1, float polinom2[], unsigned int grad2, float produs[], unsigned int &gradp)
{
	gradp = grad1 + grad2;
	for (unsigned int index1 = 0; index1 <= grad1; index1++)
		for (unsigned int index2 = 0; index2 <= grad2; index2++)
			produs[index1 + index2] += polinom1[index1] * polinom2[index2];
}

void afisare(float produs[], unsigned int gradp)
{
	std::cout << "Coeficientii polinomului produs sunt: ";
	std::cout << "\n";
	for (unsigned int index = 0; index <= gradp; index++)
		std::cout << produs[index] << " ";
	std::cout << "\n";
}

int main()
{
	float polinom1[55], polinom2[55], produs[55];
	unsigned int grad1, grad2, gradp;
	citire(polinom1, grad1);
	citire(polinom2, grad2);
	produs_polin(polinom1, grad1, polinom2, grad2, produs, gradp);
	afisare(produs, gradp);
	system("pause");
	return 0;
}