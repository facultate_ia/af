﻿/*34. Să se verifice daca cifrele lui x sunt in progresie aritmetica.*/

#include <iostream>

bool verific(unsigned int x)
{
	if (x < 100)
		return 0;
	else
		while (x > 99)
		{
			if (x / 10 % 10 != (x % 10 + x / 100 % 10) / 2)
				return 0;
			x /= 10;
		}
	return 1;
}

int main()
{
	unsigned int x;
	std::cin >> x;
	if (verific(x))
		std::cout << "Da";
	else
		std::cout << "Nu";
	system("pause");
	return 0;
}