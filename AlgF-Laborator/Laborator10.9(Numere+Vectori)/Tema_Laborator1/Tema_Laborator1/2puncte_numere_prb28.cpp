﻿/*28. Să se determine reprezentarea în baza 2 a unui număr natural n.*/

#include <iostream>

unsigned int transform_baza2(unsigned int n,unsigned int baza2[])
{
	unsigned int m = 0;
	while (n)
	{
		baza2[m++] = n % 2;
		n /= 2;
	}
	return m;
}

void inversez(unsigned int baza2[], unsigned int dim)
{
	for (unsigned int stanga = 0, dreapta = dim - 1; stanga < dreapta; stanga++, dreapta--)
	{
		unsigned int aux = baza2[stanga];
		baza2[stanga] = baza2[dreapta];
		baza2[dreapta] = aux;
	}
}

void afisare(unsigned int dim, unsigned int baza2[])
{
	for (unsigned int index = 0; index < dim; index++)
		std::cout << baza2[index];
	std::cout << "\n";
}

int main()
{
	unsigned int n, baza2[100000], dim;
	std::cout << "Introduceti nr natural: ";
	std::cin >> n;
	dim = transform_baza2(n, baza2);
	inversez(baza2, dim);
	std::cout << "Nr in baza 2 este: ";
	afisare(dim, baza2);
	system("pause");
	return 0;
}