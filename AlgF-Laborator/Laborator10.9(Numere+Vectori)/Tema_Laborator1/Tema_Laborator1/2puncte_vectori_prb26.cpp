﻿/*26. Să se determine primele două maxime dintr-un vector (printr-o singură parcurgere).*/

#include <iostream>

void citire(unsigned int &dim, int vector[])
{
	std::cout << "Indroduceti dimensiunea vectorului: ";
	std::cin >> dim;
	std::cout <<"Introduceti elementele vectorului: ";
	for (unsigned int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

int det_maxim(unsigned int dim,int vector[],int maxim1,int &maxim2)
{
	for (unsigned int index = 0; index < dim; index++)
		if (vector[index] > maxim1)
		{
			maxim2 = maxim1;
			maxim1 = vector[index];
		}
		else
			if (vector[index] > maxim2)
				maxim2 = vector[index];
	return maxim1;
}

int main()
{
	int vector[100], maxim1 = INT64_MIN, maxim2 = INT64_MIN;
	unsigned int dim;
	citire(dim, vector);
	maxim1 = det_maxim(dim, vector, maxim1, maxim2);
	std::cout << "Primul maxim este: " << maxim1 << "\n" << "Al doilea maxim este: " << maxim2 << "\n";
	system("pause");
	return 0;
}