/*3.Se da o matrice patratica cu elem natrurale sa se interschimbe elem simetrice fata de diag princ care au aceeasi paritate.*/

#include <iostream>

void citire(unsigned int &n, int matrice[100][100])
{
	std::cout << "Introduceti nr de linii si de coloane: ";
	std::cin >> n;
	std::cout << "\nIntroduceti matricea: \n";
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			std::cin >> matrice[i][j];
}

void afisare(unsigned int n, int matrice[100][100])
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			std::cout << matrice[i][j] << " ";
		std::cout << "\n";
	}
}

void schimb(unsigned int n, int matrice[100][100])
{
	for(int i=1;i<n;i++)
		for(int j=0;j<i;j++)
			if (matrice[i][j] % 2 == matrice[j][i] % 2)
			{
				int aux = matrice[i][j];
				matrice[i][j] = matrice[j][i];
				matrice[j][i] = aux;
			}
}

int main()
{
	unsigned int n;
	int matrice[100][100];
	citire(n, matrice);
	schimb(n, matrice);
	std::cout << "Matricea schimbata este: \n";
	afisare(n, matrice);
	system("pause");
	return 0;
}