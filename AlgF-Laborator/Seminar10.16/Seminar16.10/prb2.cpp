/*2.Se citeste o matrice patratica cu n linii, n coloane, cu elem nr nat din intervalul [0,1000]. 
Ordonati cresc elem de pe diag principala prin interschimbari de linii si coloane.*/

#include <iostream>

void citire(int matrice[100][100], int nr_linii, int nr_coloane)
{
	for (int index1 = 0; index1 < nr_linii; index1++)
		for (int index2 = 0; index2 < nr_coloane; index2++)
			std::cin >> matrice[index1][index2];
}

void afisare(int matrice[100][100], int nr_linii, int nr_coloane)
{
	for (int index1 = 0; index1 < nr_linii; index1++)
	{
		for (int index2 = 0; index2 < nr_coloane; index2++)
			std::cout << matrice[index1][index2] << " ";
		std::cout << std::endl;
	}
}

int main()
{
	int matrice[100][100], dim, index1, index2, index3, auxiliar;
	std::cin >> dim;
	citire(matrice, dim, dim);
	for (index1 = 0; index1 < dim - 1; index1++)
		for (index2 = index1; index2 < dim; index2++)
			if (matrice[index1][index1] > matrice[index2][index2])
			{
				for (index3 = 0; index3 < dim; index3++)
				{
					auxiliar = matrice[index3][index1];
					matrice[index3][index1] = matrice[index3][index2];
					matrice[index3][index2] = auxiliar;
				}
				for (index3 = 0; index3 < dim; index3++)
				{
					auxiliar = matrice[index1][index3];
					matrice[index1][index3] = matrice[index2][index3];
					matrice[index2][index3] = auxiliar;
				}
			}
	std::cout << std::endl;
	afisare(matrice, dim, dim);
	system("pause");
	return 0;
}