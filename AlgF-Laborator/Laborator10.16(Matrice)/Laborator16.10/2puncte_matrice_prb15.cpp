﻿/*15. Să se ordoneze crescator doar elementele prime aflate pe diagonala principala.*/

#include <iostream>

void citire(unsigned int &n, int matrice[100][100])
{
	std::cout << "Introduceti nr de linii si de coloane: ";
	std::cin >> n ;
	std::cout << std::endl << "Introduceti matricea: " << std::endl;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			std::cin >> matrice[i][j];
}

void afisare(unsigned int n, int matrice[100][100])
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			std::cout << matrice[i][j] << " ";
		std::cout << std::endl;
	}
}

bool prim(int x)
{
	if (x < 2 || x>2 && x % 2 == 0)
		return 0;
	for (int d = 3; d*d <= x; d += 2)
		if (x%d == 0)
			return 1;
}

void sortare(unsigned int dim, int matrice[100][100])
{
	for (int i = 0; i < dim; i++)
		if (prim(matrice[i][i]))
		{
			for (int j = 0; j < i; j++)
				if (prim(matrice[j][j]) && matrice[i][i] < matrice[j][j])
				{
					int aux = matrice[i][i];
					matrice[i][i] = matrice[j][j];
					matrice[j][j] = aux;
				}
			for (int j = i + 1; j < dim; j++)
				if (prim(matrice[j][j]) && matrice[i][i] > matrice[j][j])
				{
					int aux = matrice[i][i];
					matrice[i][i] = matrice[j][j];
					matrice[j][j] = aux;
				}
		}
}

int main()
{
	unsigned int n;
	int matrice[100][100], vector[100];
	citire(n, matrice);
	sortare(n, matrice);
	std::cout << std::endl;
	afisare(n, matrice);
	system("pause");
	return 0;
}