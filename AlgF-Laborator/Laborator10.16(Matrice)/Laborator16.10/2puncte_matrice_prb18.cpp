﻿/*18. Să se șteargă liniile din matrice care au toate elementele numere cu suma cifrelor pară.*/

#include <iostream>

void citire(unsigned int &n, unsigned int &m, int matrice[100][100])
{
	std::cout << "Introduceti nr de linii si de coloane: ";
	std::cin >> n >> m;
	std::cout << std::endl << "Introduceti matricea: " << std::endl;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			std::cin >> matrice[i][j];
}

void afisare(unsigned int n, unsigned int m, int matrice[100][100])
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			std::cout << matrice[i][j] << " ";
		std::cout << std::endl;
	}
}

bool verific(int i, unsigned int m, int matrice[100][100])
{
	int suma = 0;
	for (int j = 0; j < m; j++)
		suma += matrice[i][j];
	return suma % 2;
}

void sterg(unsigned int n, unsigned int m, int matrice[100][100])
{
	for (int i = 0; i < n-1; i++)
	{
		if (!verific(i, m, matrice))
			for (int k = i; k < n; k++)
				for (int j = 0; j < m; j++)
					matrice[k][j] = matrice[k+1][j];
	}
	if (!verific(n - 1, m, matrice))
	{
		n--;
	}
}

int main()
{
	unsigned int n, m;
	int matrice[100][100];
	citire(n, m, matrice);
	sterg(n, m, matrice);
	std::cout << std::endl << "Matricea nou este:" << std::endl;
	afisare(n, m, matrice);
	system("pause");
	return 0;
}