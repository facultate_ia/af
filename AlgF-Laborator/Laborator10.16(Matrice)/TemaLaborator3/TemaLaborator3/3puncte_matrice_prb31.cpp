﻿/*31. Să se roteasca matricea cu 90 de grade, in sens trigonometric.*/

#include <iostream>

void read(unsigned int &dim, int matrix[100][100])
{
	std::cin >> dim;
	for (int index = 0; index < dim; index++)
		for (int index2 = 0; index2 < dim; index2++)
			std::cin >> matrix[index][index2];
}

void rotation90(unsigned int dim, int matrix[100][100])
{
	int vector[100];
	for (unsigned int index = 0; index < dim / 2; index++)
	{
		for (unsigned int index2 = index; index2 < dim - index; index2++)
		{
			vector[index2] = matrix[index][index2];
			matrix[index][index2] = matrix[index2][dim - index - 1];
		}
		for (unsigned int index2 = dim - index - 1; index2 > index; index2--)
			matrix[dim - index2 - 1][dim - index - 1] = matrix[dim - index - 1][index2];
		for (unsigned int index2 = dim - index - 1; index2 > index; index2--)
		{
			matrix[dim - index - 1][index2] = matrix[index2][index];
			matrix[index2][index] = vector[dim - index2 - 1];
		}
	}
}
 
void write(unsigned int dim, int matrix[100][100])
{
	for (unsigned int index = 0; index < dim; index++)
	{
		for (unsigned int index2 = 0; index2 < dim; index2++)
			std::cout << matrix[index][index2] << " ";
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

int main()
{
	unsigned int dim;
	int matrix[100][100];
	read(dim, matrix);
	rotation90(dim, matrix);
	write(dim, matrix);
	system("pause");
	return 0;
}