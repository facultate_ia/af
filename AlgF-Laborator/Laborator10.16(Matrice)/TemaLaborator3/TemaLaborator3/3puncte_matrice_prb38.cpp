﻿/*38. Se consideră o matrice pătratică A de dimensiuni nxn, subdiagonală. O matrice se numeşte subdiagonală 
dacă toate elementele aflate deasupra diagonalei principale sunt nule.
Observaţie: suma şi produsul a două matrice subdiagonale este tot o matrice subdiagonală.
a. Să se transforme partea utilă a matricii (adică elementele de pe diagonala
principala şi de sub diagonala principală) într-un vector.
b. Să se scrie un algoritm care citeşte 2 matrice subdiagonale A şi B, le transformă
conform (a.) în doi vectori Va şi Vb şi apoi calculează produsul C=AB al celor
două matrice folosind doar vectorii Va şi Vb.*/

#include <iostream>
struct Triplet
{
	int val;
	int poz_vector;
	int nr_submultime;
	int poz_submultime;
};
int matrix3[100][100];

void read(unsigned int &dim, int matrix[100][100])
{
	std::cin >> dim;
	for (int index = 0; index < dim; index++)
		for (int index2 = 0; index2 < dim; index2++)
			std::cin >> matrix[index][index2];
}

int a_transform(unsigned int dim, int matrix[100][100],Triplet vector[])
{
	vector[100];
	unsigned int dimv = 0, nr = 1;
	for (int index = 0; index < dim; index++)
		for (int index2 = 0; index2 <= index; index2++)
		{
			vector[dimv].val = matrix[index][index2];
			vector[dimv].poz_vector = dimv;
			vector[dimv].nr_submultime = (dimv == 0) ? 1 : (vector[dimv - 1].nr_submultime == vector[dimv - 1].poz_submultime + 1) ? ++nr : nr;
			vector[dimv].poz_submultime = (dimv == 0) ? 0 : (vector[dimv - 1].nr_submultime > vector[dimv - 1].poz_submultime + 1) ? vector[dimv - 1].poz_submultime ++ : 0;
			dimv++;
		}
	return dimv;
}

void b_inmultire_matrice(Triplet vector_a[], Triplet vector_b[], unsigned int dimv)
{
	for (int index1 = 0; index1 < dimv; index1++)
		for (int index2 = 0; index2 < vector_a[index1].nr_submultime; index2++)
		{
			matrix3[index1][index2] += vector_a[vector_a[index1].poz_submultime].val * vector_b[vector_b[index1].poz_submultime].val;
		}
}

void write(unsigned int dim, int matrix[100][100])
{
	for (int index = 0; index < dim; index++)
	{
		for (int index2 = 0; index2 < dim; index2++)
			std::cout << matrix[index][index2] << " ";
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

int main()
{
	Triplet vector_a[600], vector_b[600];
	unsigned int dim, dimv;
	int matrix1[100][100], matrix2[100][100];
	read(dim, matrix1);
	read(dim, matrix2);
	dimv = a_transform(dim, matrix1, vector_a);
	dimv = a_transform(dim, matrix2, vector_b);

	system("pause");
	return 0;
}