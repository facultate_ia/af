﻿/*4. Se consideră un sir de numere intregi. Să se scrie o funcție care construiește două stive, una cu numerele negative 
și una cu cele pozitive, care conțin numerele în ordinea inițială, folosind doar structuri de tip stivă.*/
#include <iostream>
struct MyStack
{
	unsigned int dim;
	int elements[100], vf = -1;

	bool isEmpty()
	{
		if (vf == -1)
			return true;
		return false;
	}

	bool isFull()
	{
		if (vf >= 100 || vf >= dim)
			return true;
		return false;
	}

	void push(int element)
	{
		if (!isFull())
		{
			elements[++vf] = element;
			dim++;
		}
	}

	void pop()
	{
		if (!isEmpty())
		{
			vf--;
			dim--;
		}
	}

	void print()
	{
		for (int index = 0; index < dim; index++)
			std::cout << elements[index] << " ";
		std::cout << std::endl;
	}
};

void read_vector(unsigned int &dim, int vector[])
{
	std::cin >> dim;
	for (int index = 0; index < dim; index++)
		std::cin >> vector[index];
}

void construction_stacks(unsigned int dim_vector, int vector[], MyStack &nr_poz, MyStack &nr_neg)
{
	for (int index = dim_vector - 1; index > 0; index++)
		if (vector[index] < 0)
			nr_neg.push(vector[index]);
		else
			nr_poz.push(vector[index]);
}

int main()
{
	MyStack nr_poz, nr_neg;
	int vector[100];
	unsigned int dim_vector;
	read_vector(dim_vector, vector);
	construction_stacks(dim_vector, vector, nr_poz, nr_neg);
	nr_neg.print();
	nr_poz.print();
	system("pause");
	return 0;
}