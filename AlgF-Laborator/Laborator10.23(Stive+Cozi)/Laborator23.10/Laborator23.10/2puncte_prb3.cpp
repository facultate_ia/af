﻿/*3. Să se verifice dacă un șir de paranteze rotunde, citite într-un șir de caractere, este corect (eventual utilizând o stivă).
parantezare corecta (()(()())())
parantezare incorecta ((()((*/
#include <iostream>
#include <string>

struct MyStack
{
	int elements[100], vf = -1;

	bool isEmpty()
	{
		if (vf == -1)
			return true;
		return false;
	}

	bool isFull()
	{
		if (vf >= 100)
			return true;
		return false;
	}

	void push(int element)
	{
		if (!isFull())
		{
			elements[++vf] = element;
		}
	}

	void pop()
	{
		if (!isEmpty())
		{
			vf--;
		}
	}

	void print()
	{
		for (int i = 0; i <= vf; i++)
			std::cout << elements[i] << std::endl;
	}
};

void read(char string[])
{
	std::cin.getline(string, 30);
}

bool parantezare_corecta(char string[])
{
	MyStack obj;
	int index;
	for (index = 0; index < strlen(string); index++)
		if (string[index] == '(')
			obj.push(string[index]);
		else
		{
			if (!obj.isEmpty())
				obj.pop();
			else
				break;
		}
	if (obj.isEmpty() && index==strlen(string))
		return true;
	return false;
}

int main()
{
	char string[31];
	read(string);
	if (parantezare_corecta(string))
		std::cout << "Da";
	else
		std::cout << "Nu";
	std::cout << std::endl;
	system("pause");
	return 0;
}