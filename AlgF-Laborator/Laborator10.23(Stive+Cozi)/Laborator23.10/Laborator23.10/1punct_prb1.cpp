﻿/*1. Să se implementeze toate operațiile caracteristice unei stive (push, pop, isEmpty, isFull).*/
#include <iostream>

struct MyStack
{
	int elements[100], vf = -1;

	bool isEmpty()
	{
		if (vf == -1)
			return true;
		return false;
	}

	bool isFull()
	{
		if (vf >= 100)
			return true;
		return false;
	}

	void push(int element)
	{
		if (!isFull())
		{
			elements[++vf] = element;
		}
	}

	void pop()
	{
		if (!isEmpty())
		{
			vf--;
		}
	}

	void print()
	{
		for (int i = 0; i <= vf; i++)
			std::cout << elements[i] << std::endl;
	}
};

int main()
{
	MyStack obj;
	int x;
	for (int i = 0; i < 5; i++)
	{
		std::cin >> x;
		obj.push(x);
	}
	obj.print();
	obj.pop();
	obj.print();
	if (obj.isEmpty())
		std::cout << "Yes, the stack is empty!" << std::endl;
	else
		std::cout << "No, the stack is not empty!" << std::endl;
	if (obj.isFull())
		std::cout << "Yes, the stack is full!" << std::endl;
	else
		std::cout << "No, the stack is not full!" << std::endl;
	system("pause");
	return 0;
}