﻿/*3. Să se testeze că un număr este palindrom (se folosește structura stivă).*/
#include <iostream>
struct MyStack
{
	int dim;
	int elements[100], vf = -1;

	bool isEmpty()
	{
		if (vf == -1)
			return true;
		return false;
	}

	bool isFull()
	{
		if (vf >= 100 || vf >= dim)
			return true;
		return false;
	}

	void push(int element)
	{
		if (!isFull())
		{
			elements[++vf] = element;
			dim++;
		}
	}

	void pop()
	{
		if (!isEmpty())
		{
			vf--;
			dim--;
		}
	}

	void print()
	{
		for (int index = 0; index < dim; index++)
			std::cout << elements[index] << " ";
		std::cout << std::endl;
	}
};

int nr_cifre(int x)
{
	int nr = 0;
	do
	{
		nr++;
		x / 10;
	} while (x);
	return nr;
}

bool verificare_palindrom(int x, int nr, MyStack obj)
{
	int index = 0;
	while (index < nr / 2)
	{
		obj.push(x % 10);
		x /= 10;
		index++;
	}
	while (x)
	{
		if (x % 10 == obj.elements[obj.vf])
		{
			obj.pop();
			x /= 10;
		}
		else
			return false;
	}
	return true;
}

int main()
{
	MyStack obj;
	int x, nr;
	std::cin >> x;
	nr = nr_cifre(x);
	if (verificare_palindrom(x, nr, obj))
		std::cout << "Yes, the number is palindrome!" << std::endl;
	else
		std::cout << "No, the number isn't palindrome!" << std::endl;
	system("pause");
	return 0;
}