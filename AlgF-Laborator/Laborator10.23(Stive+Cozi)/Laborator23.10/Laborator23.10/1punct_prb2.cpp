﻿/*2. Să se implementeze toate operațiile caracteristice unei cozi (push, pop, isEmpty, isFull).*/
#include <iostream>
#define SizeMax 100
struct MyQueue
{
	int elements[10], first = 0, last = 0;

	bool isEmpty()
	{
		if (first >= last)
			return true;
		return false;
	}

	bool isFull()
	{
		if (last >= SizeMax)
			return true;
		return false;
	}

	void push(int element)
	{
		if (!isFull())
		{
			elements[last++] = element;
		}
	}

	void pop()
	{
		if (!isEmpty())
		{
			first++;
		}
	}

	void print()
	{
		for (int index = first; index < last; index++)
			std::cout << elements[index] << std::endl;
	}
};

int main()
{
	MyQueue obj;
	int x;
	for (int i = 0; i < 5; i++)
	{
		std::cin >> x;
		obj.push(x);
	}
	obj.print();
	obj.pop();
	obj.print();
	if (obj.isEmpty())
		std::cout << "Yes, the queue is empty!";
	else
		std::cout << "No, the queue is not empty!";
	if (obj.isFull())
		std::cout << "Yes, the queue is full!";
	else
		std::cout << "No, the queue is not full!";
	system("pause");
	return 0;
}