/*Se da forma poloneza postfixata a unei expresii matematice. Sa se evalueze expresia (folosind stive).*/
#include <iostream>
#include <string>

struct MyStack
{
	int dim = 0;
	int elements[100], vf = -1;

	bool isEmpty()
	{
		if (vf == -1)
			return true;
		return false;
	}

	bool isFull()
	{
		if (vf >= 100 || vf >= dim)
			return true;
		return false;
	}

	void push(int element)
	{
		if (!isFull())
		{
			elements[++vf] = element;
			dim++;
		}
	}

	void pop()
	{
		if (!isEmpty())
		{
			vf--;
			dim--;
		}
	}

	void print()
	{
		for (int index = 0; index < dim; index++)
			std::cout << elements[index] << " ";
		std::cout << std::endl;
	}
};

void read(char string[])
{
	std::cin.getline(string, 30);
}

bool validare(char *string)
{
	for (int index = 0; index < strlen(string); index++)
		if (!strchr("0123456789+-*/", string[index]))
			return false;
	return true;
}

int evaluare_expresie(char expresie[], MyStack obj)
{
	for (int index = 0; index < strlen(expresie); index++)
		if (strchr("0123456789", expresie[index]))
			obj.push(expresie[index] - '0');
		else
		{
			int y = obj.elements[obj.vf];
			obj.pop();
			int x = obj.elements[obj.vf];
			obj.pop();
			if (expresie[index] == '+')
				obj.push(x + y);
			else
				if (expresie[index] == '-')
				{
					if (x < y)
					{
						int aux = x;
						x = y;
						y = aux;
					}
					obj.push(x - y);
				}
				else
					if (expresie[index] == '*')
						obj.push(x * y);
					else
					{
						if (y != 0)
							obj.push(x / y);
						else
							std::cout << "Nu se poate imparti la 0";
					}
		}
	return obj.elements[obj.vf];
}

int main()
{
	char expresie[31];
	MyStack obj;
	read(expresie);
	if (validare(expresie))
		evaluare_expresie(expresie, obj);
	else
		std::cout << "Expresia este invalida!" << std::endl;
	system("pause");
	return 0;
}