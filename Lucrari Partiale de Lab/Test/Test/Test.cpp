#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
std::ifstream fin("C:\\Users\\ro_ad\\Desktop\\Test\\Test\\Text.txt");

struct Cartiere
{
	int noStrazi;
	int *strada;
};

void citire(int &noCartiere, Cartiere *cartier)
{
	int index = 0;
	while (index < noCartiere)
	{
		while (fin.get())
		{
			cartier[index].noStrazi = 0;
			int x;
			fin >> x;
			cartier[index].strada[cartier[index].noStrazi++] = x;
		}
		index++;
	}
}

void a_cautare_strazi(int noCartiere,Cartiere *cartier)
{
	for (int index1 = 0; index1 < noCartiere; index1++)
	{
		int index2 = 0;
		for (int index2 = 0; index2 < cartier[index1].noStrazi; index2++)
		{
			if (cartier[index1].strada[index2] > 500)
				std::cout << index1 << "-" << index2 << "-" << cartier[index1].strada[index2] << std::endl;
		}
	}
}

void quick_sort(int stanga, int dreapta, int *vector)
{
	int index1 = stanga, index2 = dreapta, x = vector[(stanga + dreapta) / 2];
	do {
		while (vector[index1] > x)
			index1++;
		while (vector[index2] < x)
			index2--;
		if (index1 <= index2)
		{
			int aux = vector[index1];
			vector[index1] = vector[index2];
			vector[index2] = aux;
			index1++;
			index2--;
		}
	} while (index1 <= index2);
	if (stanga < index2)
		quick_sort(stanga, index2, vector);
	if (dreapta > index1)
		quick_sort(index1, dreapta, vector);
}

void b_sortare_strazi(int noCartiere, Cartiere *cartier)
{
	for (int index = 0; index < noCartiere; index++)
		quick_sort(0, cartier[index].noStrazi - 1, cartier[index].strada);
}

int populatie_cartier(int *strazi, int dim)
{
	int populatie = 0;
	for (int index = 0; index < dim; index++)
		populatie += strazi[index];
	return populatie;
}

void c_cautare_cartier(int noCartiere, Cartiere *cartier)
{
	int maxim = 0, pozitie = -1;
	for(int index=0;index<noCartiere;index++)
		if (populatie_cartier(cartier[index].strada, cartier[index].noStrazi) >= maxim)
		{
			maxim = populatie_cartier(cartier[index].strada, cartier[index].noStrazi);
			pozitie = index;
		}
	std::cout << pozitie << "-" << maxim;
}

int main()
{
	srand((int)time(0));
	int noCartiere= rand() % 12 + 2;
	Cartiere *cartier;
	cartier = new Cartiere[noCartiere];
	cartier->strada = new int[100];
	citire(noCartiere, cartier);
	a_cautare_strazi(noCartiere, cartier);
	b_sortare_strazi(noCartiere, cartier);
	c_cautare_cartier(noCartiere, cartier);
	system("pause");
	return 0;
}