/*Se considera o matrice patratica A de dimensiuni n?n, sudiagonala. O matrice se
numeste subdiagonala daca toate elementele aflate deasupra diagonalei principale sunt
nule.
Observatie: suma si produsul a doua matrice subdiagonale este tot o matrice
subdiagonala.
a. Sa se transforme parte utila a matricii (adica elementele de pe diagonala
principal si de sub diagonala principala) �ntr-un vector.
b. Sa se scrie un algoritm care citeste 2 matrice subdiagonale A si B, le transforma
conform (a) �n doi vectori Va si Vb si apoi calculeaza produsul C=AB al celor
doua matrice folosind doar vectorii Va si Vb.*/
#include<iostream>
struct triplet
{
	int val, pozgrup, nrgrup;
};

void citireMatrice(int X[100][100],int n)

{
	int i, j;
	
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			std::cout << "Dati elementul [" << i << "][" << j << "]: ";
			std::cin >> X[i][j];
		}
	}

}
void creareVector(triplet X[100], int Mat[100][100], int n)
{
	int i, lung = -1, j;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j <= i; j++)
		{
			lung++;
			X[lung].val = Mat[i][j];
			X[lung].nrgrup = i+1;
			X[lung].pozgrup = j + 1;
		}
	}
}
void ProdusMatrice(triplet a[100], triplet b[100],triplet c[100], int lunVec, int n)
{
	int i,j;
		for (i = 0; i < lunVec; i++)
		{
			c[i].val= 0;
		}
		
		for (i = 0; i <lunVec; i++)
		{
			for (j = 1; j <= a[i].pozgrup; j++)
			{
				c[(a[i].nrgrup - 1)*a[i].nrgrup / 2 + j - 1].val += a[(a[i].nrgrup - 1)*a[i].nrgrup / 2 + a[i].pozgrup - 1].val*b[(a[i].pozgrup - 1)*a[i].pozgrup / 2 + j - 1].val;
				

			}
		}
		
}
void AfisareMatrice(int X[100][100], int n)
{
	int i, j;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			std::cout << X[i][j] << " ";
		}
		std::cout << std::endl;
	}
}
int main()
{
	int A[100][100], B[100][100],n,lunVec,i,C[100][100],j,k;
	triplet a[100], b[100], c[100];
	std::cout << "Dati n: ";
	std::cin >> n;
	citireMatrice(A, n);
	citireMatrice(B, n);
	creareVector(a, A, n);
	creareVector(b, B, n);
	lunVec = n*(n + 1) / 2;
	ProdusMatrice(a, b, c, lunVec, n);
	k = -1;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j <n; j++)
		{
			if (j <= i)
			{
				k++;
				C[i][j] = c[k].val;
			}
			else
			{
				C[i][j] = 0;
			}

		}
	}

	AfisareMatrice(C, n);
	std::cout << std::endl;
	
		
		system("pause");
}