ORDINE:

LINIA 17 MATRICI
LINIA 120 VECTORI
LINIA 179 ALGORITMI FUNDAMENTALI
LINIA 279 SORTARI
LINIA 438 CAUTARE BINARA SI INTERCLASARE
LINIA 489 RANDOMIZARE
LINIA 506 STIVE SI COZI







//------------------ - !--MATRICE--!------------------------

parcurgeri:
__________________________________________________
principala : for (i = 0; i < n; i++)	matrice[i][i]
secundara : for (i = 0; i < n; i++)	mat[i][n - i - 1]

deasupra principala :
for (i = 0; i < n; i++)
	for (j = i + 1; j < n; j++)
		printf(�%d�, a[i][j]);

sub principala :
for (i = 0; i < n; i++)
	for (j = 0; j < i; j++)
		printf(�%d�, a[i][j]);

deasupra secundara :
for (i = 0; i < n - 1; i++)
	for (j = 0; j < n - i - 1; j++)
		printf(�%d�, a[i][j]);

sub secundara :
for (i = 1; i < n; i++)
	for (j = n - i; j < n; j++)
		printf(�%d�, a[i][j]);
__________________________________________________


spirala :

void spirala(int a[20][20], int n)		//afiseaza in spirala
{			// daca vrei de construit, inlocuiesti cout cu un vector in care se baga numerele
	int lc = 0, cc = 0, i;		// ex: faci progresie aritmetica. etc

	while (n >= 0)
	{
		for (i = cc; i < n; i++)
			cout << a[lc][i] << " ";
		for (i = lc + 1; i < n; i++)
			cout << a[i][n - 1] << " ";
		for (i = n - 2; i >= cc; i--)
			cout << a[n - 1][i] << " ";
		for (i = n - 2; i >= lc + 1; i--)
			cout << a[i][cc] << " ";
		n--;
		lc++;
		cc++;
	}
}

triunghiuri:

Pentru NORD : i + j<n + 1 si i<j
Pentru SUD : i + j>n + 1 si i>j
Pentru EST : i + j > n + 1 si i < j
Pentru VEST : i + j<n + 1 si i>j
_________________________________________________

Rotire dreapta  90 in alta matrice :	//intr-o matrice noua

for (i = 0; i < m; i++)
	for (j = 0; j < n; j++)
		rot90_dreapta[i][j] = a[n - 1 - j][i];


Rotire stanga 90 in alta matrice :		//intr-o matrice noua

for (i = 0; i < m; i++)
	for (j = 0; j < n; j++)
		rot90_stanga[i][j] = a[j][m - 1 - i];
___________________________________________________

//citire :

void citire(int matrice[100][100], unsigned int &lungime) {
	std::cin >> lungime;
	for (unsigned int index1 = 0; index1 < lungime; index1++)
		for (unsigned int index2 = 0; index2 < lungime; index2++)
			std::cin >> matrice[index1][index2];
}


//afisare:

void afisare(int matrice[100][100], unsigned int lungime)
{

	for (unsigned int index1 = 0; index1 < lungime; index1++) {

		for (unsigned int index2 = 0; index2 < lungime; index2++)

			std::cout << matrice[index1][index2] << " ";

		std::cout << "\n";

	}

}



//---------------------- - !--VECTORI--!---------------------------- -


void citire_vector(int vector[], unsigned int &n)
{
	std::cin >> n;
	for (int index = 0; index < n; index++)
		std::cin >> vector[index];
}

void afisare_vector(int vector[], unsigned int &n)
{
	std::cin >> n;
	for (int index = 0; index < n; index++)
		std::cout << vector[index] << " ";

}
void inserare_numar(int a[], int &n, int numar, int poz)	// pe o anumita pozitie inserezi un numar
{
	n++;
	for (int i = n; i >= poz; i--)
		a[i] = a[i - 1];
	a[poz - 1] = numar;
}

void stergere_numar(int a[], int &n, int element)		//sterge un numar daca indeplineste conditia
{
	int i = 0;
	while (i < n)
	{
		if (element == a[i])			// de aici ( aici am pus sa stearga daca gaseste unu egal)
		{
			while (i < n)
			{
				a[i] = a[i + 1];
				i++;
			}
			n--;
		}
		else
			i++;
	}
}
void sortare(int a[100], int n)			//o sortare de cacat pentru un vector, daca nu trebuie eficienta
{
	int aux;
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (a[j] < a[i]) {				//pui conditii usor aici
				aux = a[i];
				a[i] = a[j];
				a[j] = aux;
			}
}





//----------------!--ALGORITMI FUNDAMENTALI--!---------------------


bool prim(int x)
{
	if (x < 2 || x>2 && x % 2 == 0)
		return false;
	for (int index = 3; index*index <= x; index = index + 2)
		if (x%index == 0)
			return false;
	return true;
}

int suma_cifre(int x)
{
	int s = 0;
	while (x)
	{
		s += x % 10;
		x /= 10;
	}
	return s;
}

int oglindit(int x)
{
	int  cif = 0;
	while (x)
	{
		cif = cif * 10 + x % 10;
		x /= 10;
	}
	return cif;
}

int prima_cifra(int x)
{
	while (x > 9)
		x /= 10;
	return x;
}

void divizori_numar(int x)
{
	std::cout << "divixorii numarului sunt: ";
	for (int index = 2; index <= x; index++)
		if (x%index == 0)
			std::cout << index << " ";
	std::cout << '\n';
}

int suma_divizori(int x)
{
	int s = 0;
	for (int index = 2; index <= x; index++)
		if (x%index == 0)
			s += index;
	return s;
}

int cmmdc(int a, int b)
{
	while (a != b)
	{
		if (a > b)
			a = a - b;
		else b = b - a;
	}
	return a;
}

int cmmmc(int a, int b)
{
	return (a*b) / cmmdc(a, b);
}

int suma_factori_primi(int n)
{
	int suma = 0, d = 2;
	while (n > 1)
	{
		int p = 0;
		while (n % d == 0)
		{
			++p;
			n /= d;
		}
		if (p)
			suma += d;
		++d;
		if (n > 1 && d * d > n)
		{
			d = n;
		}
	}
	return suma;
}



//-------------------!!--SOTRTARI--!!------------------------



//-----------------SORTARE DE STRUCTURA CU QUICKSORT

#include <iostream>
#include <fstream>
#include <algorithm>
std::ifstream f("Text.txt");

struct data {
	unsigned int an;
	unsigned int luna;
	unsigned int zi;
};
bool ordine(data data1, data data2)
{
	if (data1.an > data2.an)
		return true;
	if (data1.an < data2.an)
		return false;
	if (data1.luna > data2.luna)
		return true;
	if (data1.luna < data2.luna)
		return false;
	if (data1.zi > data2.zi)
		return true;
	if (data1.zi < data2.zi)
		return false;
	return false;
}
void citire(int &lungime, data vectDate[20])
{
	f >> lungime;
	for (int i = 0; i < lungime; i++)
	{
		f >> vectDate[i].an >> vectDate[i].luna >> vectDate[i].zi;
	}
}
void interschimbare(data &data1, data &data2)
{
	data aux;
	aux.an = data1.an;
	aux.luna = data1.luna;
	aux.zi = data1.zi;
	data1.an = data2.an;
	data1.luna = data2.luna;
	data1.zi = data2.zi;
	data2.an = aux.an;
	data2.luna = aux.luna;
	data2.zi = aux.zi;

}
void quickSort(int st, int dr, data vect[30])
{
	int i = st;
	int j = dr;
	data x = vect[(st + dr) / 2];
	do {
		while (ordine(vect[i], x) == true && i < dr)i++;
		while (ordine(vect[j], x) == false && j > st)j--;
		if (i <= j)
		{
			interschimbare(vect[i], vect[j]);
			i++;
			j--;
		}
	} while (i <= j);
	if (st < j)
		quickSort(st, j, vect);
	if (dr > i)
		quickSort(i, dr, vect);
}
void afisare(int lungime, data vectDate[20])
{
	for (int i = 0; i < lungime; i++)
	{
		std::cout << vectDate[i].an << " " << vectDate[i].luna << " " << vectDate[i].zi << "\n";
	}
}
int main()
{
	data vectDate[20];
	int lungime;
	citire(lungime, vectDate);
	quickSort(0, lungime - 1, vectDate);
	afisare(lungime, vectDate);
	system("pause");
	return 0;
}


//------------------------QUICKSORT

void afisare(int lungime, int vect[30])
{
	for (int i = 0; i < lungime; i++)
	{
		std::cout << vect[i] << " ";
	}
}
void citire(int &lungime, int vect[30])
{
	f >> lungime;
	for (int i = 0; i < lungime; i++)
		f >> vect[i];
}
void quickSort(int st, int dr, int vect[30])
{
	int i = st;
	int j = dr;
	int x = vect[(st + dr) / 2];
	do {
		while (vect[i] < x)i++;
		while (vect[j] > x)j--;
		if (i <= j)
		{
			int aux = vect[i];
			vect[i] = vect[j];
			vect[j] = aux;
			i++;
			j--;
		}
	} while (i <= j);
	if (st < j)
		quickSort(st, j, vect);
	if (dr > i)
		quickSort(i, dr, vect);
}

//----------------SORTARE PRIN SELECTIE

void sortarePrinSelectie(int lungime, int vect[30])
{
	for (int i = 0; i < lungime - 1; i++)
	{
		int minim = vect[i];
		int poz = i;
		for (int j = i + 1; j < lungime; j++)
		{
			if (minim > vect[j])
			{
				minim = vect[j];
				poz = j;
			}
		}
		if (poz != i)
		{
			int aux = vect[i];
			vect[i] = vect[poz];
			vect[poz] = aux;
		}
	}
}




//------------------!--INTERCLASARE SI CAUTARE BINARA--!-----------------------\

void interclasare(int vect1[30], int vect2[30], int vectRez[60], int lungime1, int lungime2, int &lungimeRez)
{
	int i = 0;
	int j = 0;
	lungimeRez = 0;
	while (i < lungime1 && j < lungime2)
	{
		if (vect1[i] <= vect2[j])
		{
			vectRez[lungimeRez] = vect1[i];
			i++;
			lungimeRez++;
		}
		else
		{
			vectRez[lungimeRez] = vect2[j];
			j++;
			lungimeRez++;
		}
	}
	for (; i < lungime1; i++)
	{
		vectRez[lungimeRez] = vect1[i];
		lungimeRez++;
	}
	for (; j < lungime2; j++)
	{
		vectRez[lungimeRez] = vect2[j];
		lungimeRez++;
	}
}

//-----------------------------------------

bool cautareBinara(int vect[30], int lungime, int st, int dr, int nrCautat)
{
	if (st < dr)
	{
		int mij = (st + dr) / 2;
		if (vect[mij] == nrCautat)
			return 1;
		if (vect[mij] < nrCautat)
			cautareBinara(vect, lungime, mij + 1, dr, nrCautat);
		else
			cautareBinara(vect, lungime, dr, mij - 1, nrCautat);
	}
	return 0;
}

//-----------------RANDOMIZARE-------------------


//CREARE VECTOR DE LUNGIME RANDOM

#include <time.h>

void creare_vector_randomizat(int &lungime, int vect[10])
{
	srand(time(NULL));
	lungime = rand() % 10 + 0;
	std::cout << lungime << std::endl;
	for (int i = 0; i < lungime; i++)
		vect[i] = rand() % 300 + 0;
}


//---------------------STIVE SI COZI----------------------------



//-----------------------CITIRE STIVA


struct Mystack
{
	int dim = 100;
	int elements[100];
	int vf = 0;
	bool isFull()
	{
		if (vf > dim)
			return 1;
		return 0;
	}
	bool isEmpty()
	{
		if (vf == 0)
			return 1;
		else
			return 0;
	}

	void push(int element)
	{
		if (isFull() == 0)
		{
			elements[vf] = element;
			vf++;
		}

	}
	void pop()
	{
		if (isEmpty() == 0)
			vf--;
	}
	int top()
	{
		return elements[vf - 1];
	}
	void print()
	{
		for (int i = vf - 1; i >= 0; i--)
			std::cout << elements[i] << " ";
		std::cout << std::endl;
	}

};




//-------------------------COADA


struct MyQueue
{
	int end = -1;
	int start = 0;
	int size = 20;
	char date[100];
	void push(char element)
	{
		if (isFull() == 0)
		{
			end++;
			date[end] = element;
		}
	}
	void pop()
	{
		start++;
	}
	bool isFull()
	{
		if (end == size - 1)
			return 1;
		return 0;
	}
	bool isEmpty()
	{
		if (end == -1)
			return 1;
		return 0;
	}
	char top()
	{
		return date[start];
	}
	void afisare()
	{
		for (int i = start; i <= end; i++)
			std::cout << date[i];
		std::cout << std::endl;
	}
};